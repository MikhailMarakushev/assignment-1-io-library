section .text


; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60 ; номер системного вызова функции выхода
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax ; счетчик
.loop:
    cmp byte [rdi+rax], 0 ; сравниваем с 0 символы последовательно
    je .end ; если дошли до терминатора, заканчиваем
    inc rax ; увеличиваем счётчик
    jmp .loop
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi ; сохраняем calle-saved
    call string_length ; в rax будет лежать длина
    pop rsi ; достаем значение
    mov rdx, rax
    mov rax, 1 ; номер вызова write
    mov rdi, 1 ; задаем stdout
    syscall
    xor rax, rax
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi ; сохраняем calle-saved
    mov rax, 1
    mov rdi, 1
    mov rdx, 1
    mov rsi, rsp ; кладем адрес стека
    syscall
    pop rdi ; достаем значение
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, '\n' ; код символа
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r8, 0
    mov r9, 10
    mov rax, rdi

.loop:
    xor rdx, rdx
	div r9 ; делим на основание системы
	push rdx ; сохраняем в стек остаток от деления
	inc r8 ; увеличиваем счетчик
	cmp rax, 0 ; если число закончилось, идем на печать
	jne .loop
.additional:
    pop rdx ; достаем из стека
	add rdx, 0x30 ; переводим в нужное нам значение
    mov rdi, rdx
	push r8
	call print_char ; выводим символ
	pop r8
	dec r8
    jnz .additional
	ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    mov rax, 0
    cmp rdi, 0 ; проверяем на нулевом значении
    jl .minus ; если меньше, то чисо отрицательное
    jmp print_uint
.minus:
    push rdi ; сохраняем rdi
    mov rdi, '-'
    call print_char ; печатаем '-'
    pop rdi ; достаем rdi
    neg rdi ; берем отрицательное
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    mov rcx, 0
.loop:
    mov r11b, byte[rsi+rcx] ; записываем первый символ слова
    cmp byte[rdi+rcx], r11b ; сравниваем с первым символом другого слова
    jne .stop ; если не равны, то сразу заканчиваем
    inc rcx
    cmp r11b, 0 ; если дошли до нуля-терминатора, то успех
    jne .loop
    mov rax, 1
    ret
.stop:
    mov rax, 0
    ret

    

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rdi, rdi ; записываем stdout
    xor rax, rax ; номер вызова read
    mov rdx, 1
    push 0 ; место для чтения
    mov rsi, rsp ; адрес вершины стека
    syscall
    pop rax ; достаем введенный символ
    ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    mov r8, 0
.loop:
    cmp r8, rsi ; сравнение размера строки и счетчика
    jge .stop ; если больше или равно, то закончили
    push rdi ; сохраняем нужные регистры
    push rsi
    push r8
    call read_char ; читаем символ   
    pop r8 ; достаем значения
    pop rsi
    pop rdi
    cmp al, '\n'
    je .spaces
    cmp al, ' '
    je .spaces
    cmp al, `\t`
    je .spaces
    mov byte[rdi+r8],al ; берем символ и записываем его в буффер
    cmp al, 0 ; равниваем с 0
    je .ok
    inc r8
    jmp .loop
.spaces:
    cmp r8, 0 
    je .loop ; пропускаем пробельные символы, если они в начале строки
.ok:
    mov rdx, r8
    mov rax, rdi
    ret
.stop:
    mov rax, 0
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	mov r8, 0 ; счетчик
	mov rax, 0
	mov r10, 10 ; делитель
.loop:
	mov r9, 0
	mov r9b, byte[rdi + r8]
    cmp r9b, 0x30 	; проверяем, что чисо находится в диапазоне 9-0
	jb .end		
	cmp r9b, 0x39		
	ja .end						
	inc r8			
	sub r9b, '0'		
	mul r10			
	add rax, r9		
	jmp .loop
.end:
	mov rdx, r8		
	ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
.sign:
    mov al, byte[rdi] 
    cmp al, '-' ; проверяем на отрицательное число            		
    jne .plus	; если положительное, просто печатаем
    inc rdi                 		
.check: 			
    mov al, byte[rdi]
    cmp al, '0' ; смотрим, находится ли число между 9 и 0
    jl .another
    cmp al, '9' ; смотрим, находится ли число между 9 и 0
    jg .another
    push rdi ; сохраняем rdi
   	call parse_uint 			
   	pop rdi ; достаем rdi
   	cmp rdx, 0 ; проверяем на нулевое значение
   	je .another
   	neg rax 			
   	inc rdx 			
   	ret
.plus:
    push rdi 				
    call parse_uint
    pop rdi 				
    ret   
.another:
    xor rax, rax
    xor rdx, rdx
    ret  

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    mov rax, 0 ; счетчик
    call string_length
    cmp rax, rdx ; проверим, чтобы буфер был больше слова
    jge .fail
    mov r8, 0
.loop:
    mov dl, byte [rdi + r8] ; возьмем символ из одной строки
    mov byte [rsi + r8], dl ; закинем в буфер
    cmp dl, 0 ; если дошли до нуля, то строка закончилась
    je .end
    inc r8
    jmp .loop
.fail:
    xor rax, rax
.end:
    ret
